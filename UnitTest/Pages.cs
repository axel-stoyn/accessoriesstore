﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Domain.Abstract;
using Moq;
using Domain.Organization;
using Web.Controllers;
using System.Collections.Generic;
using System.Linq;
using Web.Helpers;
using Web.Models;


namespace UnitTest
{
    [TestClass]
    public class Pages
    {
        [TestMethod]
        public void Create_Page()
        {
            // arrange
            Mock<IAccessory> mock = new Mock<IAccessory>();
            mock.Setup(m => m.Accessories).Returns(new List<Accessory>
            {
                new Accessory { AccessoryId = 1, Name = "1"},
                new Accessory { AccessoryId = 2, Name = "2"},
                new Accessory { AccessoryId = 3, Name = "3"},
                new Accessory { AccessoryId = 4, Name = "4"},
                new Accessory { AccessoryId = 5, Name = "5"}
            });
            AccessoryController controller = new AccessoryController(mock.Object);
            controller.pageSize = 3;

            //act
            AccessoriesList result = (AccessoriesList)controller.List(null, 2).Model;

            // assert
            List<Accessory> accessories = result.Accessories.ToList();
            Assert.IsTrue(accessories.Count == 2);
            Assert.AreEqual(accessories[0].Name, "4");
            Assert.AreEqual(accessories[1].Name, "5");
        }
    }
}
