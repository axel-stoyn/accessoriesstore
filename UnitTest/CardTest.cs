﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Domain.Organization;
using System.Linq;
using static Domain.Organization.Card;

namespace UnitTest
{
    /// <summary>
    /// Summary description for CardTest
    /// </summary>
    [TestClass]
    public class CardTest
    {
        [TestMethod]
        public void AddNew()
        {
            Accessory accessory1 = new Accessory { AccessoryId = 1, Name = "test1" };
            Accessory accessory2 = new Accessory { AccessoryId = 2, Name = "test2" };

            Card card = new Card();

            card.Add(accessory1, 1);
            card.Add(accessory2, 1);

            List<CardLine> results = card.Cards.ToList();

            Assert.AreEqual(results.Count(), 2);
            Assert.AreEqual(results[0].Accessory, accessory1);
            Assert.AreEqual(results[1].Accessory, accessory2);
        }

        [TestMethod]
        public void CardTotal()
        {
            Accessory accessory1 = new Accessory { AccessoryId = 1, Name = "test1", Cost = 20 };
            Accessory accessory2 = new Accessory { AccessoryId = 2, Name = "test2", Cost = 5 };

            Card card = new Card();

            card.Add(accessory1, 1);
            card.Add(accessory2, 1);
            card.Add(accessory1, 3);
            decimal result = card.TotalCost();

            Assert.AreEqual(result, 85);
        }
    }
}
