﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Domain.Abstract;
using Domain.Organization;
using Web.Controllers;
using System.Linq;

namespace UnitTest
{
    /// <summary>
    /// Summary description for Navigation
    /// </summary>
    [TestClass]
    public class Navigation
    {
        [TestMethod]
        public void Create_Categotes()
        {
            // arrange
            Mock<IAccessory> mock = new Mock<IAccessory>();
            mock.Setup(m => m.Accessories).Returns(new List<Accessory>
            {
                new Accessory { AccessoryId = 1, Name = "1", Cathegory = "test1"},
                new Accessory { AccessoryId = 2, Name = "2", Cathegory = "test2"}
            });

            //act
            MenuNavigationController nav = new MenuNavigationController(mock.Object);

            string categotySelect = "test2";
            string result = nav.Menu(categotySelect).ViewBag.SelectedCategory;

            // assert
            Assert.AreEqual(categotySelect, result);
        }
    }
}
